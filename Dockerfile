FROM alpine:latest

RUN apk update && apk --no-cache add \
	apache2 \
	php7 \
	php7-apache2 \
	php7-session \
	php7-exif \
	php7-mbstring \
	php7-gd \
	php7-mysqli \
	php7-json \
	php7-zip \
	php7-imagick \
	git \
	ffmpeg \
	composer \
	imagemagick

RUN cd /var/www/localhost/htdocs && \
	rm index.html && \
	git clone --recurse-submodules https://github.com/LycheeOrg/Lychee.git && \
	mv Lychee/* . && rm -rf .git* && \
	composer install && \
	touch dist/user.css && \
	chmod -R 775 uploads dist && \
	chmod -R 750 data && \
	chown -R apache:apache uploads data

CMD	[ "httpd", "-D", "FOREGROUND" ]